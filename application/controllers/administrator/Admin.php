<?php
    defined('BASEPATH') or exit('Not Allowed Direct Access');

    class Admin extends CI_Controller{
        public function __construct(){
            parent::__construct();
        }
        public function dashboard(){
            $this->load->view('_partial/layout');
        }
        public function index(){
            $this->load->view('admin/admin-panel');
        }
    }
?>