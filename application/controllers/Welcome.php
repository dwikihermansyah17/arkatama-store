<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->model('M_heroUnit');
		//parsel data/agar tampil di LP
		$data['hero'] = $this->M_heroUnit->getHero();
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit();
		$this->load->view('landing-page', $data);
	}

	public function tambahFoto()
	{
		$this->load->view('tambah-foto');
	}

	public function uploadFoto()
	{
		
                $config['upload_path']          = './uploads/';//tempat menyimpan foto
                $config['allowed_types']        = 'gif|jpg|png';//file apa saja yg boleh diupload
                $config['max_size']             = 1000;
                $config['max_width']            = 2048;
                $config['max_height']           = 1080;

                $this->load->library('upload', $config);//load libbray

                if ( ! $this->upload->do_upload('file_foto'))
                { //proses pengecekan
                        $error = array('error' => $this->upload->display_errors());

						// echo'<pre>';
						// print_r($error);
						// echo'</pre>';
						// die;

                        //$this->load->view('upload_form', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
						
						echo'<pre>';
						print_r($data);
						echo'</pre>';
						die;
                       // $this->load->view('upload_success', $data);
                }
        }
	
	
}